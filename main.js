// Game state
let currentPlayer = "X";
let gameBoard = ["", "", "", "", "", "", "", "", ""];

// DOM elements
const cells = document.querySelectorAll(".cell");
const resetButton = document.getElementById("reset-button");
const flyingCats = document.getElementById("flying-cats");

// Add event listeners
cells.forEach((cell, index) => {
  cell.addEventListener("click", () => {
    if (gameBoard[index] === "") {
      makeMove(index);
      updateBoard();
      checkWinner();
      switchPlayer();
    }
  });
});

resetButton.addEventListener("click", resetGame);

// Add flying cats
for (let i = 0; i < 10; i++) {
  const cat = document.createElement("img");
  cat.src = "https://source.unsplash.com/200x200/?cat"; // replace with the path to your cat image
  cat.classList.add("cat");
  cat.style.bottom = `${Math.random() * 100}vh`;
  cat.style.animationDuration = `${Math.random() * 2 + 3}s`;
  flyingCats.appendChild(cat);
}

// Function to make a move
function makeMove(index) {
  gameBoard[index] = currentPlayer;
}

// Function to update the game board
function updateBoard() {
  cells.forEach((cell, index) => {
    cell.textContent = gameBoard[index];
  });
}

// Function to check for a winner or a tie
function checkWinner() {
  const winningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let combination of winningCombinations) {
    const [a, b, c] = combination;
    if (
      gameBoard[a] !== "" &&
      gameBoard[a] === gameBoard[b] &&
      gameBoard[a] === gameBoard[c]
    ) {
      alert(`Player ${currentPlayer} wins!`);
      resetGame();
      return;
    }
  }

  if (!gameBoard.includes("")) {
    alert("It's a tie!");
    resetGame();
  }
}

// Function to switch players
function switchPlayer() {
  currentPlayer = currentPlayer === "X" ? "O" : "X";
}

// Function to reset the game
function resetGame() {
  currentPlayer = "X";
  gameBoard = ["", "", "", "", "", "", "", "", ""];
  updateBoard();
}
